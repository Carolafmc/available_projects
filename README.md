# List of projects

Projects for the final examination are related to the following topics: Pandemia, Pollutants, Climate Change, Systematic Review. **In case you are not able to download the paper please contact professor.**

**Note**: Each project can be developed by one student or by a small group of 2/3 students.  

Identifier|Title URL|Year|Data|State|Badge Number(BN)1|Badge Number(BN)2|Badge Number(BN)3
-|-|-|-|-|-|-|-
Project (PR) Identifier| Title and URL|Year of paper publication|Available|Available|University Badge Numbers of student 1 involved in the project|University Badge Numbers of student 2 involved in the project|University Badge Numbers of student 3 involved in the project

* The project can be developed by no more than three students.
* Data can be available in a specific project called **Project (PR) Identifier**. If not, please contact professor.
* State of the project can be Available, Not Available and Done.

SARS-COV-2 Pandemic

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR1**](https://gitlab.com/90477_mls_4ds_ii/pr1)|[Applying a hierarchical clustering on principal components approach to identify different patterns of the SARS-CoV-2 epidemic across Italian regions](https://www.nature.com/articles/s41598-021-86703-3)|2021|Available|**Done**|1076123|1083555|1095387
[**PR2**](https://gitlab.com/90477_mls_4ds_ii/pr2)|[Country transition index based on hierarchical clustering to predict next COVID-19 waves](https://www.nature.com/articles/s41598-021-94661-z)|2021|Available|Not Available|0001037640|NA|NA
[**PR3**](https://gitlab.com/90477_mls_4ds_ii/pr3)|[A machine learning and clustering-based approach for county-level COVID-19 analysis](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0267558)|2022|Available|Not Available|0001027021|0001026110|NA
[**PR4**](https://gitlab.com/90477_mls_4ds_ii/pr4)|[Clustering analysis of countries using the COVID-19 cases dataset](https://www.sciencedirect.com/science/article/pii/S2352340920306818)|2020|Available|Not Available|0001082417|0001083637|0001075402
[**PR5**](https://gitlab.com/90477_mls_4ds_ii/pr5)|[COVID-19 Cases and Deaths in Southeast Asia Clustering using K-Means Algorithm](https://iopscience.iop.org/article/10.1088/1742-6596/1783/1/012027/pdf)|2021|Available|**Done**|1076917|1082965|1080943
[**PR6**](https://gitlab.com/90477_mls_4ds_ii/pr6)|[The application of K-means clustering for province clustering in Indonesia of the risk of the COVID-19 pandemic based on COVID-19 data](https://link.springer.com/article/10.1007/s11135-021-01176-w)|2021|Available|**Done**|0001076290|0001083092|0001086078
[**PR7**](https://gitlab.com/90477_mls_4ds_ii/pr7)|[The nexus between COVID-19 deaths, air pollution and economic growth in New York state: Evidence from Deep Machine Learning](https://www.sciencedirect.com/science/article/pii/S0301479721003030)|2021|Not Available Search on WOA|Not Available|0001075289|1900105061|0001075606
[**PR8**](https://gitlab.com/90477_mls_4ds_ii/pr8)|[COVID-19 lockdown only partially alleviates health impacts of air pollution in Northern Italy](https://iopscience.iop.org/article/10.1088/1748-9326/abd3d2)|2021|Available|Not Available|0001083330|0001076664|
[**PR9**](https://gitlab.com/90477_mls_4ds_ii/pr9)|[COVID-19 Future Forecasting Using Supervised Machine Learning Models](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9099302)|2020|Available|Not Available|1083078|1084558|NA
[**PR10**](https://gitlab.com/90477_mls_4ds_ii/pr10)|[Predicting the Disease Outcome in COVID-19 Positive Patients Through Machine Learning: A Retrospective Cohort Study With Brazilian Data](https://www.frontiersin.org/articles/10.3389/frai.2021.579931/full)|2021|Available|Not Available|0001076329|NA|NA


Pollutants/Climate Change

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR50**](https://gitlab.com/90477_mls_4ds_ii/pr50)|[Air pollution prediction with machine learning: a case study of Indian cities](https://link.springer.com/content/pdf/10.1007/s13762-022-04241-5.pdf)|2022|Available|Not Available|0001038412|0001083046|0001102556
[**PR51**](https://gitlab.com/90477_mls_4ds_ii/pr51)|[A Machine Learning Approach to Predict Air Quality in California](https://downloads.hindawi.com/journals/complexity/2020/8049504.pdf)|2020|Available|**Done**|0001075746|0001086265|0001095557
[**PR52**](https://gitlab.com/90477_mls_4ds_ii/pr52)|[A machine learning approach to address air quality changes during the COVID-19 lockdown in Buenos Aires, Argentina](https://essd.copernicus.org/preprints/essd-2021-318/)|2021|Available|**Done**|0001075256|NA|NA
[**PR53**](https://gitlab.com/90477_mls_4ds_ii/pr53)|[Applying Machine Learning to Weather and Pollution Data Analysis for a Better Management of Local Areas: The Case of Napoli, Italy](https://pdfs.semanticscholar.org/4b84/b3040ca7b82295fe35c126c1fabf48486905.pdf?_ga=2.255456776.1039647219.1669135166-373685778.1669135166)|2021|Available|**Done**|1900106538|NA|NA
[**PR54**](https://gitlab.com/90477_mls_4ds_ii/pr54)|[Modeling Air Pollution, Climate, and Health Data Using Bayesian Networks: A Case Study of the English Regions](https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1002/2017EA000326)|2018|Available|Not Available|0001046794|NA|NA
[**PR55**](https://gitlab.com/90477_mls_4ds_ii/pr55)|[Prediction of air quality in Jakarta during the COVID-19 outbreak using long short-term memory machine learning](https://iopscience.iop.org/article/10.1088/1755-1315/704/1/012046/pdf)|2021|Available|Not Available|0001030267|0001038155|NA
[**PR56**](https://gitlab.com/90477_mls_4ds_ii/pr56)|[Combining Cluster Analysis of Air Pollution and Meteorological Data with Receptor Model Results for Ambient PM2.5 and PM10](https://www.mdpi.com/1660-4601/17/22/8455)|2020|Available|Not Available|0001077185|0001086141|0001036429
[**PR57**](https://gitlab.com/90477_mls_4ds_ii/pr57)|[Hierarchical Clustering for Optimizing Air Quality Monitoring Networks](https://link.springer.com/content/pdf/10.1007/978-3-030-22055-6.pdf)|2020|Available|Not Available|0001094963|NA|NA
PR58|||||NA|NA|NA

Software Quality

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR100**](https://gitlab.com/90477_mls_4ds_ii/pr100)|[Impact of Feature Selection Methods on the Predictive Performance of Software Defect Prediction Models: An Extensive Empirical Study](https://www.mdpi.com/2073-8994/12/7/1147)|2020|Available|**Done**|0001035571|0001039200|NA
[**PR101**](https://gitlab.com/90477_mls_4ds_ii/pr101)|[Performance Analysis of Feature Selection Methods in Software Defect Prediction: A Search Method Approach](https://www.mdpi.com/2076-3417/9/13/2764)|2019|Available|Not Available|0001086325|0001076451|NA
[**PR102**](https://gitlab.com/90477_mls_4ds_ii/pr102)|[Evaluating the impact of feature selection consistency in software prediction](https://www.sciencedirect.com/science/article/abs/pii/S0167642321001088)|2022|Available|Not Available|0001092678|NA|NA
[**PR103**](https://gitlab.com/90477_mls_4ds_ii/pr103)|[Investigating the effect of dataset size, metrics sets, and feauture selection techniques on software fault prediction problem](https://www.researchgate.net/publication/222124541_Investigating_the_effect_of_dataset_size_metrics_sets_and_feature_selection_techniques_on_software_fault_prediction_problem)|2008|Available|Not Available|0001103544|0001084138|0001092284
[**PR104**](https://gitlab.com/90477_mls_4ds_ii/pr104)|[Comparison of Machine Learning Techniques for Software Quality Prediction](https://www.igi-global.com/gateway/article/252885#pnlRecommendationForm)|2020|Available|Not Available|0000979198|NA|NA
[**PR105**](https://gitlab.com/90477_mls_4ds_ii/pr105)|[Multiple-classifiers in software quality engineering: Combining predictors to improve software fault prediction ability](https://www.sciencedirect.com/science/article/pii/S2215098619308390)|2020|Available|Not Available|0001076226|NA|NA
PR106|||||NA|NA|NA

Systematic Review

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR150**](https://gitlab.com/90477_mls_4ds_ii/pr150)|[Natural language processing was effective in assisting rapid title and abstract screening when updating systematic reviews](https://www.jclinepi.com/article/S0895-4356\(21\)00014-7/fulltext)|2021|Available|Not Available|0001033235|0001087967|NA
[**PR151**](https://gitlab.com/90477_mls_4ds_ii/pr151)|[A real-world evaluation of the implementation of NLP technology in abstract screening of a systematic review](https://www.medrxiv.org/content/10.1101/2022.02.24.22268947v1.full.pdf)|2022|Available|**Done**|0001052383|0001036807|NA
[**PR152**](https://gitlab.com/90477_mls_4ds_ii/pr152)|[A systematic review of natural language processing applied to radiology reports](https://bmcmedinformdecismak.biomedcentral.com/articles/10.1186/s12911-021-01533-7)|2021|Available|Not Available|0001047924|NA|NA
PR153|||||NA|NA|NA

Social Media for pandemia and climate change

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR200**](https://gitlab.com/90477_mls_4ds_ii/pr200)|[Using Twitter for sentiment analysis towards AstraZeneca/Oxford, Pfizer/BioNTech and Moderna COVID-19 vaccines](https://pmj.bmj.com/content/98/1161/544)|2022|Contact Professor|Not Available|0000987789|0001082805|NA
[**PR201**](https://gitlab.com/90477_mls_4ds_ii/pr201)|[Social media information sharing for natural disaster response](https://link.springer.com/article/10.1007/s11069-021-04528-9)|2021|Available|**Done**|0001085128|0001076882|NA
[**PR202**](https://gitlab.com/90477_mls_4ds_ii/pr202)|[Twitter, disasters and cultural heritage: A case study of the 2015 Nepal earthquake](https://onlinelibrary.wiley.com/doi/epdf/10.1111/1468-5973.12333)|2020|Contact Professor|Not Available|0001086388|NA|NA
[**PR203**](https://gitlab.com/90477_mls_4ds_ii/pr203)|[Detecting Natural Hazard-Related Disaster Impacts with Social Media Analytics: The Case of Australian States and Territories](https://eprints.qut.edu.au/227361/1/sustainability_14_00810.pdf)|2022|Contact Professor|Not Available|0001039295|0001051085|0001037016
[**PR204**](https://gitlab.com/90477_mls_4ds_ii/pr204)|[TweetDIS: A Large Twitter Dataset for Natural Disasters Built using Weak Supervision](https://arxiv.org/ftp/arxiv/papers/2207/2207.04947.pdf)|2022|Contact Professor|Not Available|0001039310|0001046664|NA
[**PR205**](https://gitlab.com/90477_mls_4ds_ii/pr205)|[Twitter vs. Zika—The role of social media in epidemic outbreaks surveillance](https://www.sciencedirect.com/science/article/pii/S2211883720301234)|2021|Contact Professor|Not Available|0001085728|0001075794|NA
[**PR206**](https://gitlab.com/90477_mls_4ds_ii/pr206)|[Ebola and Localized Blame on Social Media: Analysis of Twitter and Facebook Conversations During the 2014–2015 Ebola Epidemic](https://link.springer.com/article/10.1007/s11013-019-09635-8)|2019|Contact Professor|Not Available|0001083811|NA|NA
[**PR207**](https://gitlab.com/90477_mls_4ds_ii/pr207)|[Malaria Epidemic Prediction Model by Using Twitter Data and Precipitation Volume in Nigeria](http://koreascience.or.kr/article/JAKO201919866854592.page)|2019|Contact Professor|Not Available|0001078765|0001075183|NA
[**PR208**](https://gitlab.com/90477_mls_4ds_ii/pr208)|[Topical Mining of Malaria Using Social Media. A Text Mining Approach](https://www.semanticscholar.org/paper/Topical-Mining-of-Malaria-Using-Social-Media.-A-Boit-El-Gayar/267008e972fddcf9ba4cadb9e98a2a0e09b530c5)|2020|Contact Professor|Not Available|0001101334|NA|NA
[**PR209**](https://gitlab.com/90477_mls_4ds_ii/pr209)|[Use of social media big data as a novel HIV surveillance tool in South Africa](https://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0239304&type=printable)|2020|Available|Not Available|0001097793|NA|NA
[**PR210**](https://gitlab.com/90477_mls_4ds_ii/pr210)|[Public sentiments toward COVID-19 vaccines in South African cities: An analysis of Twitter posts](https://www.frontiersin.org/articles/10.3389/fpubh.2022.987376/full)|2022|Available|Not Available|0001001271|NA|NA

**For projects PR208, PR207, PR206, PR205, PR204, PR203, PR200, you will be invited to redo the described analysis with new data.** 

Log analysis

Identifier|Title URL|Year|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-
[**PR250**](https://gitlab.com/90477_mls_4ds_ii/pr250)|[Detecting Anomaly in Big Data System Logs Using Convolutional Neural Network](http://www.cs.ucf.edu/~lwang/papers/LogCNN2018.pdf)|2018|Availabe|Not Available|0001090502|0001090173|NA
[**PR251**](https://gitlab.com/90477_mls_4ds_ii/pr251)|[Anomaly Detection in Log Files Using Selected Natural Language Processing Methods](https://www.mdpi.com/2076-3417/12/10/5089)|2022|Available|Not Available|0001076600|0001083208|0001085604
[**PR252**](https://gitlab.com/90477_mls_4ds_ii/pr252)|[Anomaly detection of software system logs based on natural language processing](https://ieeexplore.ieee.org/document/8552075)|2018|Available|**Done**|0001077478|0001091195|NA
PR253|||||NA|NA|NA




